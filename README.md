Nicholas Kuckuck npk@uoregon.edu

Modified acp_times.py, flask_brevets.py, templates/calc.html, Dockerfile
Added run.sh, .gitignore, test.py

To test run:
nosetests
located in brevets dir

To test web page:
sudo ./run.sh
go to localhost:5000

when you type brevet distance open and close times are automatically populated
