from acp_times import open_time, close_time

def test_open_time1():
    assert open_time(50, 200, "2019-01-01 00:00") == "2019-01-01T01:28:14.117647+00:00"

def test_open_time2():
    assert open_time(100, 200, "2019-01-01 00:00") == "2019-01-01T02:56:28.235294+00:00"

def test_open_time3():
    assert open_time(150, 200, "2019-01-01 00:00") == "2019-01-01T04:24:42.352941+00:00"

def test_close_time1():
    assert close_time(50, 200, "2019-01-01 00:00") == "2019-01-01T03:20:00+00:00"

def test_close_time2():
    assert close_time(100, 200, "2019-01-01 00:00") == "2019-01-01T06:40:00+00:00"

def test_close_time3():
    assert close_time(150, 200, "2019-01-01 00:00") == "2019-01-01T10:00:00+00:00"
