"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    start_time = arrow.get(brevet_start_time, 'YYYY-MM-DD hh:mm')
    new_brevet_dist = int(brevet_dist_km)

    if new_brevet_dist <= control_dist_km:
        if new_brevet_dist <= 200:
            ret_start_time = start_time.shift(hours = 200/34)
        elif new_brevet_dist > 200 & new_brevet_dist <= 400:
            ret_start_time = start_time.shift(hours = 200/34 + 100/32)
        elif new_brevet_dist > 400 & new_brevet_dist <= 600:
            ret_start_time = start_time.shift(hours = 200/34 + 200/32)
        elif new_brevet_dist > 600 & new_brevet_dist <= 1000:
            ret_start_time = start_time.shift(hours = 200/34 + 200/32 + 200/30)
        elif new_brevet_dist > 1000 & new_brevet_dist <= 1300:
            ret_start_time = start_time.shift(hours = 200/34 + 200/32 + 200/30 + 400/28)
    
    else:
        if control_dist_km <= 200:
            ret_start_time = start_time.shift(hours = control_dist_km/34)
        elif control_dist_km > 200 & control_dist_km <= 400:
            ret_start_time = start_time.shift(hours = 200/34 + (control_dist_km-200)/32)
        elif control_dist_km > 400 & control_dist_km <= 600:
            ret_start_time = start_time.shift(hours = 200/34 + 200/32 + (control_dist_km-600)/28)
    

    return ret_start_time.isoformat()
    


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    min_speed = {"1":(200,15), "2":(200,15), "3":(200,15), "4":(400,11.428), "5":(300,13.333)}
    start_time = arrow.get(brevet_start_time, 'YYYY-MM-DD hh:mm')
    new_brevet_dist = int(brevet_dist_km)

    if new_brevet_dist <= 200:
        speed = min_speed['1'][1]
        close_time = start_time.shift(hours = control_dist_km/speed)
    elif new_brevet_dist > 200 & new_brevet_dist <= 400:
        speed = min_speed['2'][1]
        close_time = start_time.shift(hours = control_dist_km/speed)
    elif new_brevet_dist > 400 & new_brevet_dist <= 600:
        speed = min_speed['3'][1]
        close_time = start_time.shift(hours = control_dist_km/speed)
    elif new_brevet_dist > 600 & new_brevet_dist <= 1000:
        speed = min_speed['4'][1]
        close_time = start_time.shift(hours = control_dist_km/speed)
    elif new_brevet_dist > 1000 & new_brevet_dist <= 1300:
        speed = min_speed['5'][1]
        close_time = start_time.shift(hours = control_dist_km/speed)

    return close_time.isoformat()
